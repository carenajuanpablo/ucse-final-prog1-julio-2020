﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lógica
{
    public class Muñeca : Ortesis
    {
        public bool esUniversal { get; set; }
        public bool tieneSoportePulgar { get; set; }
        public bool permiteMovimientoActivo { get; set; }

        public override int ObtenerPorcentajeRecargo()
        {
            return tieneSoportePulgar ? base.ObtenerPorcentajeRecargo()+3 : base.ObtenerPorcentajeRecargo();
        }
        public override string ObtenerDescripciónPersonalizada()
        {
            if (tieneSoportePulgar)
            {
                return $"{nombre}  |  Tiene Soporte pulgar  |";
            }
            else
            {
                return $"{nombre}  |  No tiene Soporte pulgar  |";
            }
        }
    }
}
