﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lógica
{
    public enum Color
    {
        blanco, azul, verde, negro
    }
    public enum Material
    {
        termoplástico, venda, mixto
    }
    public abstract class Ortesis
    {
        public string codigo { get; set; }
        public Color color { get; set; }
        public Material materialConfección { get; set; }
        public string nombre { get; set; }
        public decimal precioUnitario { get; set; }
        public int stock { get; set; }
        public string caracteristicas { get; set; }

        public virtual int ObtenerPorcentajeRecargo()
        {
            return materialConfección == Material.termoplástico ? 3 : 0;
        }

        public abstract string ObtenerDescripciónPersonalizada();

    }
}
