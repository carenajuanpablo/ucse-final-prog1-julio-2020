﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lógica
{
    class Institucion
    {
        List<Cliente> ListaClientes;
        List<Ortesis> ListaOrtesis;
        List<Venta> ListaVentas;
        List<Localidad> ListaLocalidades;
        List<Provincia> ListaProvincias;


        public Institucion()
        {
            ListaClientes = new List<Cliente>();
            ListaOrtesis = new List<Ortesis>();
            ListaVentas = new List<Venta>();
            ListaLocalidades = new List<Localidad>();
            ListaProvincias = new List<Provincia>();
        }

           //CORRECCION: LO CORRECTO HUBIERA SIDO DEVOLVER UN MENSAJE COMO SOLICITABA EL ENUNCIADO
        public bool RegistrarNuevaOrtesis(string cod, Ortesis nuevaOrtesis)
        {
            bool resultado = true;
            var Ortesis = ListaOrtesis.Find(x => x.codigo == cod);
            if (Ortesis != null)
            {
                ListaOrtesis.Add(Ortesis);
            }
            else
            {
                resultado = false;
            }
            return resultado;
        }

        public void RegistrarVenta(string _codOrtesis, int _dniCliente)
        {
            var Ortesis = ListaOrtesis.Find(x => x.codigo == _codOrtesis);
            var Cliente = ListaClientes.Find(x => x.dni == _dniCliente);
            if ((Ortesis != null) & (Cliente != null) & (Ortesis.stock > 0))
            {

                Venta nuevaVenta = new Venta();
                nuevaVenta.FechaVenta = DateTime.Now;
                nuevaVenta.dni = _dniCliente;
                nuevaVenta.codigoOrtesis = _codOrtesis;
                nuevaVenta.porcentajeRecargoAplicado = Ortesis.ObtenerPorcentajeRecargo();
                nuevaVenta.precioTotalCalculado = Ortesis.precioUnitario + (Ortesis.precioUnitario * Ortesis.ObtenerPorcentajeRecargo() / 100);

                ListaVentas.Add(nuevaVenta);
                Ortesis.stock -= 1;
            }
        }

        public List<Cliente> ListadoClientes(string nombreProvincia)
        {
            //CORRECCIÓN: NO PUEDO HACER UN FIRSTORDEFAULT PORQUE PUEDE HABER MUCHAS LOCALIDADES DE ESA PROVINCIA, NECESITO TRAER TODAS Y BUSCAR LOS USUARIOS DE TODAS ESAS LOCALIDADES
            var Localidad = ListaLocalidades.FirstOrDefault(x => x.provincia.nombreProvincia == nombreProvincia);
            return ListaClientes.Where(x => x.codigoPostal == Localidad.codigoPostal).ToList();
        }

        public List<string> ListadoOrtesis(DateTime fechaDesde, DateTime fechaHasta, string nombreProvincia)
        {
            //CORRECCIÓN: NO PUEDO HACER UN FIRSTORDEFAULT PORQUE PUEDE HABER MUCHAS LOCALIDADES DE ESA PROVINCIA, NECESITO TRAER TODAS Y BUSCAR LOS USUARIOS DE TODAS ESAS LOCALIDADES
                //LO CORRECTO SERÍA USAR EL MÉTODO QUE YA CREASTE ARRIBA.
            var Localidad = ListaLocalidades.FirstOrDefault(x => x.provincia.nombreProvincia == nombreProvincia);

            List<string> lista = new List<string>();
            foreach (var item in ListaVentas)
            {
                Cliente Cliente = ListaClientes.Find(x => x.dni == item.dni);
                if ((item.FechaVenta >= fechaDesde) & (item.FechaVenta<=fechaHasta) & (Cliente.codigoPostal == Localidad.codigoPostal ))
                {
                    Ortesis Ortesis = ListaOrtesis.Find(x => x.codigo == item.codigoOrtesis);
                    string desc = $"{item.FechaVenta}  |  {Ortesis.ObtenerDescripciónPersonalizada()} |  {item.precioTotalCalculado}";
                    lista.Add(desc);
                }
            }
            return lista;
        }


    }
}
