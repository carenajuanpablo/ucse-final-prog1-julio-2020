﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lógica
{
    public class Pie_Tobillo : Ortesis
    {
        public int talle { get; set; }
        public bool esAutoajustable { get; set; }
        public bool permiteMovimientoActivo { get; set; }

        public override string ObtenerDescripciónPersonalizada()
        {
            if (esAutoajustable)
            {
                return $"{nombre}  |  Es Autoajustable  |";
            }
            else
            {
                return $"{nombre}  |  No es autoajustable  |";
            }
        }
    }
}
