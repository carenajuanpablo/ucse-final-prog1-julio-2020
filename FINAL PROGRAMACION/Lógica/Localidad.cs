﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lógica
{
    public class Localidad
    {
        public int codigoPostal { get; set; }
        public string nombreLocalidad { get; set; }
        public Provincia provincia { get; set; }
    }
}
