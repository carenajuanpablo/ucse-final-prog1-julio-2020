﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lógica
{
    public class Rodilla : Ortesis
    {
        public decimal largoTotal { get; set; }
        public bool tieneSoporteRotula { get; set; }
        public decimal diametroInferior { get; set; }
        public decimal diametroSuperior { get; set; }

        public override int ObtenerPorcentajeRecargo()
        {
            return tieneSoporteRotula ? base.ObtenerPorcentajeRecargo()+4 : base.ObtenerPorcentajeRecargo();
        }

        public override string ObtenerDescripciónPersonalizada()
        {
            if (tieneSoporteRotula)
            {
                return $"{nombre}  |  Tiene Soporte Rotula  |";
            }
            else
            {
                return $"{nombre}  |  No tiene Soporte Rotula  |";
            }

        }
    }
}
