﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lógica
{
    public class Provincia
    {
        private static int ultimoCodigo = 0;
        public int codProvincia { get; set; }
        public string nombreProvincia { get; set; }

        //CORRECCION: NO SE PUEDE INCREMENTAR EL CÓDIGO ASÍ, HAY QUE USAR LA LISTA PRINCIPAL Y SU ATRIBUTO LENGTH
        public Provincia()
        {
            codProvincia = ++ultimoCodigo;
        }
    }
}
