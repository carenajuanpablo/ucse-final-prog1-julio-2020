﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lógica
{
    class Venta
    {
        private static int UltimoCodigo = 0;
        public int codigoVenta { get; set; }
        public string codigoOrtesis { get; set; }
        public int dni { get; set; }
        public DateTime FechaVenta { get; set; }
        public int porcentajeRecargoAplicado { get; set; }
        public decimal precioTotalCalculado { get; set; }

        //CORRECCION: NO SE PUEDE INCREMENTAR EL CÓDIGO ASÍ, HAY QUE USAR LA LISTA PRINCIPAL Y SU ATRIBUTO LENGTH
        public Venta()
        {
            codigoVenta = ++UltimoCodigo;
        }
    }
}
